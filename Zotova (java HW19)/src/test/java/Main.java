/*
Заходим в UserData
Создаем пользователя (Каждый раз года мы будем запускать тест пользователь должен быть уникальным)
Логинимся в приложение
Идем в employees и находим себя и убедились то нашлось(В таблице вы находитесь)
 */



import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Main<user> {
    WebDriver driver;


    @BeforeClass
    public static void setup (){
    System.setProperty("webdriver.gecko.driver","C:\\Users\\Yulia\\Documents\\03 - learning\\Hillel\\geckodriver-v0.26.0-win64\\geckodriver.exe");
    };

    @Before
    public void precondition (){
        driver= new FirefoxDriver();


    };

    @Test
    public void openUserDataApp () throws InterruptedException {
        driver.get("https://user-data.hillel.it/html/registration.html");
        driver.findElement(By.className("registration")).sendKeys(Keys.ENTER);
        driver.findElement(By.id("first_name")).sendKeys("Yuliia");
        driver.findElement(By.id("last_name")).sendKeys("Test");
        driver.findElement(By.id("field_work_phone")).sendKeys("235678");
        driver.findElement(By.id("field_phone")).sendKeys("380501162537");
        driver.findElement(By.id("field_email")).sendKeys("Test_0121@mail.ru");
        driver.findElement(By.id("field_password")).sendKeys("Test_0102");
        driver.findElement(By.id("female")).click();
        driver.findElement(By.id("position")).sendKeys("qa");
        driver.findElement(By.id("button_account")).click();

        Alert alert = new WebDriverWait(driver,20).until(ExpectedConditions.alertIsPresent());
        alert.accept();

        driver.get("https://user-data.hillel.it/html/registration.html");
        driver.findElement(By.id("email")).sendKeys("Test_0121@mail.ru");
        driver.findElement(By.id("password")).sendKeys("Test_0102");
        driver.findElement(By.className("login_button")).click();

        driver.findElement(By.id("employees")).click();

        driver.findElement(By.id("first_name")).sendKeys("Yuliia");
        driver.findElement(By.id("search")).click();

      //  Thread.sleep(5000);
    };
    @After
    public void postCondition() {
      //  driver.quit();

    };


};


